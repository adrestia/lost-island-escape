﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMove : MonoBehaviour {

	private Rigidbody rb;
	public float moveSpeed, jumpStrength, rotateSpeed;
	private float horizontalMove, verticalMove, jumpMove;

	private void Start() 
	{
		rb = GetComponent<Rigidbody>();
		
	}

	// Update is called once per frame
	void Update () {
		GetControls();
	}

	public void GetControls()
	{
		horizontalMove = CrossPlatformInputManager.GetAxis("Horizontal");
		verticalMove = CrossPlatformInputManager.GetAxis("Vertical");
		jumpMove = CrossPlatformInputManager.GetAxis("Jump");

		if(jumpMove > 0 && Physics.Raycast(transform.position, Vector3.down, 0.3f))
			rb.AddForce(Vector3.up * jumpStrength, ForceMode.Impulse);
		
		rb.MovePosition(transform.position + transform.forward * verticalMove * moveSpeed);
		transform.Rotate(new Vector3(0, horizontalMove * rotateSpeed,0));
		
	}


}
