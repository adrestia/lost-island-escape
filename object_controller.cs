﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class object_controller : MonoBehaviour
{
    public float speed = 5.0f;
    public float drag = 0.5f;
    public float RotateSpeed = 25.0f;
    public Vector3 MoveVector{set;get;}
    public Virtual_Joystick joystick;

    public Rigidbody rb;

    // Start is called before the first frame update
    private void Start()
    {
        rb = gameObject.AddComponent<Rigidbody>();
        rb.maxAngularVelocity = RotateSpeed;
        rb.drag = drag;
    }

    // Update is called once per frame
    private void Update()
    {
        MoveVector = PoolInput();

        Move();
    }

    private void Move()
    {
        rb.AddForce((MoveVector * speed));
    }

    private Vector3 PoolInput()
    {
        Vector3 dir = Vector3.zero;
        
        // dir.x = Input.GetAxis("Horizontal");
        // dir.z = Input.GetAxis("Vertical");

        dir.x = joystick.Horizontal();
        dir.z = joystick.Vertical();

        if(dir.magnitude > 1)
        {
            dir.Normalize();
        }

        return dir;
    }
}
